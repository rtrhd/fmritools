# Alternate Brain Extraction

## optibet
### Shell Script for Performing Brain Extraction using FSL and AFNI

Use the `Makefile` to install `optibet` to `/usr/local/bin`.

The bash script `optibet` is a revised version of the [opiBET](http://montilab.psych.ucla.edu/fmri-wiki/optibet) script from Evan Lutkenhoff at Montilab. It is described in [this article](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0115551).


Usage: optibet `<options>` [-i] `<input_image>`
:    -i: input image file
:    -a: use `AFNI` for initial extraction rather than bet
:    -t: use `MNI152_T1_2mm_brain_mask` for template rather than `MNI152_T1_1mm_brain_mask`
:    -g: script uses `avg152T1_brain` for template rather than `MNI152_T1_1mm_brain_mask`
:    -d: debug mode (will NOT delete intermediate files)
:    -h: show this message


Notes:
:    script requires proper installation of `FSL` and `AFNI`
:    input image should be in standard orientation
:    use `.nii.gz` format file for input
:    outputs binarized brain-extraction mask, saved as: `<input_image>_optiBET_brain_mask.nii.gz`
:    and full-intensity brain-extraction, saved as: `<input_image>_optiBET_brain.nii.gz`


The script performs an initial extraction using FSL [bet](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BET) or Afni [3dSkullStrip](http://afni.nimh.nih.gov/pub/dist/doc/program_help/3dSkullStrip.html). It then improves on this by warping to a template and using the template structure to generate the refined mask. This involves the `FSL` tools [flirt](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT) and [fnirt](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FNIRT).

## antsbet
### A shell script using the ANTS library for brain extraction
Use the `Makefile` to install `antsbet` to `/usr/local/bin`.

The bash script `antsbet` uses the ANTS library to perform the segmentation

Usage: antsbet [-v] [-h] [-o <outdir>] [-t <template>] [-i] `<input_image>`

Options:
:    -t: name of template to use (in /usr/local/ANTs/templates)
:    -o: name of output directory (default is same as that of input file)
:    -v: verbose, print extra logging information
:    -h: print help message

Environment:
    ANTSPATH must refer to the bin directory of a working ANTs installation (/usr/local/ANTs/bin/).
    
Notes:
:    calls the ANTs script antsBrainExtraction (was antsBrainExtraction.sh)
:    script requires proper installation of `FSL` and `ANTS`
:    input image should be in standard orientation
:    use `.nii.gz` format file for input
:    outputs binarized brain-extraction mask, saved as: `<input_image>_optiBET_brain_mask.nii.gz`
:    and full-intensity brain-extraction, saved as: `<input_image>_optiBET_brain.nii.gz`


## vbm8bet
### A nipype based python script to perform brain extraction using SPM/VBM8

Use the `Makefile` to install `vbm8bet` to `/usr/local/bin`.

The python script `vbm8bet` uses the SPM `VBM8` library to perform the segmentation. It uses the python
neuroimaging pipeline tool `nipype` to handling wrapping calls to matlab for `VBM8`. 

Usage: vbm8bet `<input_image>`
:    -V: version
:    -h: usage

Notes:
:    script requires proper installation of `FSL`, `nipype`, `matlab`, `SPM8` and `VBM8` 
:    input image should be in standard orientation
:    use `.nii.gz` format file for input
:    outputs binarized brain-extraction mask, saved as: `<input_image>_optiBET_brain_mask.nii.gz`
:    and full-intensity brain-extraction, saved as: `<input_image>_optiBET_brain.nii.gz`


The script performs an initial extraction using FSL [bet](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BET) or Afni [3dSkullStrip](http://afni.nimh.nih.gov/pub/dist/doc/program_help/3dSkullStrip.html). It then improves on this by warping to a template and using the template structure to generate the refined mask. This involves the `FSL` tools [flirt](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT) and [fnirt](http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FNIRT).

R. Hartley-Davies, August 2016
 
